# Objectively Better

### Summary
Welcome to Objectively Better!

This project was started because I wanted to write a MacOS platform layer for my [Nebula Game Engine](https://github.com/KevinMiller77/Nebula).
I naturally found Apples metal-cpp project which exposes cpp hooks into the Objective-C++ runtime to make native c++ calls and work with Metal. 
I also found the metal-cpp-extensions library which was not hosted as a library on its' own but rather existed within the metal-cpp examples supplied by Apple.

When getting started using the metal-cpp and metal-cpp-exentensions library, I noticed quite a lot of functionality was not present.
I am assuming this is because Apple wants you to use Swift and Obj-C++ but I wouldn't settle for that.

This project is an open source attempt to bridge 100% of the following frameworks to c++;
[] Foundation
[] AppKit
[] QuartzCore
[] Metal
[] MetalKit